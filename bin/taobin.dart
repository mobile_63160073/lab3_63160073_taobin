import 'dart:io';
class Drinks with Sweet, Straw_Lid{
  String name ='';
  var type=['Hot','Cool','Smoothie'];
  double price=0;

  Drinks(this.name, this.type, this.price);

  String getName(){
    return name;
  }

  double getPrice(){
    return price;
  }
  
  String? getType(String typ){
    for(int i=0; i<type.length; i++){
      if(typ.contains(type[i])){
        return type[i].toString();
      }
    }
    return null;
  }

  List<String> getAllType(){
    return type;
  }

  @override
  var sweet = ['Free','Less','Normal','Sweet','VerySweet'];
  
  @override
  String? getSweet(String swt){
    for(int i=0; i<sweet.length; i++){
      if(swt.contains(sweet[i])){
        return sweet[i].toString();
      }
    }
    return null;
  }

  @override
  List<String> getAllSweet(){
    return sweet;
  }
}

mixin Sweet {
  var sweet;
  String? getSweet(String swt);
  List<String> getAllSweet();
}

mixin Straw_Lid {
  void wantStraw(){
    print('ต้องการหลอดไหมคะ(yes or no)');
  }
  void wantLid(){
    print('ต้องการฝาไหมคะ?(yes or no)');
  }
}

class Coffee extends Drinks {
  Coffee(String name, var type, double price): super(name, type, price);
  Coffee.sweet(String name, var type, double price, var sweet): super(name, type, price){
    this.sweet=sweet;
  }
}

class Milk extends Drinks {
  Milk(String name, var type, double price): super(name, type, price);
  Milk.sweet(String name, var type, double price, var sweet): super(name, type, price){
    this.sweet=sweet;
  }
}

class Tea extends Drinks {
  Tea(String name, var type, double price): super(name, type, price);
  Tea.sweet(String name, var type, double price, var sweet): super(name, type, price){
    this.sweet=sweet;
  }
}

class Protein extends Drinks {
  Protein(String name, double price): super(name, [''], price);
}
void main(List<String> arguments) {
   //mock up
  //1.ชามะนาวโซดา
  Tea lemon_tea = new Tea('Lemon Tea', ['Cool','Smoothie'], 25);
  //2.ลาเต้
  Coffee latae = new Coffee('Latae', ['Hot','Cool','Smoothie'], 35);
  //3.นมโปรตีน
  Protein milk_protein = new Protein('Milk Shake', 55);
  //4.โกโก้
  Milk cocoa = new Milk('Cocoa', ['Hot','Cool','Smoothie'], 25);
  List<Tea> list_teas = [lemon_tea];
  List<Coffee> list_coffees = [latae];
  List<Milk> list_milks = [cocoa];
  List<Protein> list_proteins = [milk_protein];

  //process
  String? menu;
  showWelcome();
 
  showMenu();
  menu = stdin.readLineSync()!;
  if(menu.contains('Tea')){
    process(list_teas);
  }
  if(menu.contains('Coffee')){
    process(list_coffees);
  }
  if(menu.contains('Milk')){
    process(list_milks);
  }
  if(menu.contains('Protein')){
    process(list_proteins);
  }

}

void showWelcome(){
  print('ตู้เต่าบินยินดีให้บริการค่ะ');
}

void showMenu() {
  print('Menu :');
  print('1.Coffee');
  print('2.Tea');
  print('3.Milk');
  print('4.Protein');
  print('เลือกประเภทเครื่องดื่ม(พิมพ์ประเภทเครื่องดื่ม)');
}

void process(List<Drinks> list_drinks) {
  String? name,type,sweet,straw,lid;
  double? price;
  showDrinks(list_drinks);
  name = stdin.readLineSync();
  
  type = chooseType(list_drinks, name!);
  
  sweet = chooseSweet(list_drinks, name);
  
  straw = needStraw(list_drinks, name);
  lid = needLid(list_drinks, name);
  
  payment(list_drinks, name);
  price = double.parse(stdin.readLineSync()!);
  change(list_drinks, name, price);
  print('================================');
  print('กรุณารับสินค้าของคุณ');
  print(toString(list_drinks, name, type, sweet));
  strawLid(straw,lid);
  print('ขอบคุณที่ใช้บริการค่ะ');
}

//All function of process

void showDrinks(List<Drinks> list){
  print('--------------------------------');
  for(int i=0; i<list.length; i++){
    print(list[i].getName());
    if(!list[i].getName().endsWith("Shake")){
      print(list[i].getAllType());
    }
    print('${list[i].getPrice()} บาท');
    print('=====================================================\n');
  }
  print('กรุณาเลือกเครื่องดื่มที่ลูกค้าต้องการบนหน้าจอได้เลยค่ะ(พิมพ์ชื่อสินค้า)');
}

String chooseType(List<Drinks> list,String name){
  if(equalProtein(list, name)){
    return '';
  }
  print('--------------------------------');
  for(int i=0; i<list.length; i++){
    if(name.contains(list[i].getName())){
      print(list[i].getAllType());
    }
  }
  print('กรุณาเลือกประเภทของเครื่องดื่ม(พิมพ์ประเภทของเครื่องดื่ม)');
  String? type = stdin.readLineSync();
  return type!;
}

String chooseSweet(List<Drinks> list,String name){
  if(equalProtein(list, name)){
    return '';
  }
  print('--------------------------------');
  for(int i=0; i<list.length; i++){
    if(name.contains(list[i].getName())){
      print(list[i].getAllSweet());
    }
  }
  print('กรุณาเลือกระดับความหวาน(พิมพ์ความหวาน)');
  String? sweet = stdin.readLineSync();
  return sweet!;
}

String needStraw(List<Drinks> list, String name){
  print('--------------------------------');
  for(int i=0; i<list.length; i++){
    if(name.contains(list[i].getName())){
      list[i].wantStraw();
    }
  }
  String? straw = stdin.readLineSync();
  return straw!;
}

String needLid(List<Drinks> list, String name){
  for(int i=0; i<list.length; i++){
    if(name.contains(list[i].getName())){
      list[i].wantLid();
    }
  }
  String? lid = stdin.readLineSync();
  return lid!;
}

void payment(List<Drinks> list, String name){
  print('--------------------------------');
  for(int i=0; i<list.length; i++){
    if(name.contains(list[i].getName())){
      print('กรุณาชำระเงิน ${list[i].getPrice()} บาท ค่ะ');
    }
  }
  print('ระบุจำนวนเงิน:');
}

void change(List<Drinks> list, String name, double price){
  for(int i=0; i<list.length; i++){
    if(name.contains(list[i].getName())){
      double change = price-list[i].getPrice();
      print('กรุณารับเงินทอน $change บาท');
    }
  }
}

void strawLid(String straw, String lid){
  if(straw.contains("Yes")&&lid.contains("Yes")){
    print('กรุณารับหลอดและฝาด้วยค่ะ');
  }else if(straw.contains("Yes")){
    print('กรุณารับหลอดด้วยค่ะ');
  }else if(lid.contains("Yes")){
    print('กรุณารับฝาด้วยค่ะ');
  }
}

bool equalProtein(List<Drinks> list, String name){
  for(int i=0; i<list.length; i++){
    if(name.contains(list[i].getName())){
      if(list[i].getName().endsWith('Shake')){
        return true;
      }
    }
  }
  return false;
}

@override
String? toString(List<Drinks> list, String name, String type, String sweet){
  for(int i=0; i<list.length; i++){
    if(name.contains(list[i].getName())){
      if(equalProtein(list, name)){
        return '$name';
      }
      return '${list[i].getType(type)} $name (Sugar: ${list[i].getSweet(sweet)})';
    }
  }
   return null;
}